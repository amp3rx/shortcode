<?php
/**
* Plugin Name: Date Shortcode
* Version: 0.0.1 BETA
* Author: Ionel Vaidianu
*/


add_shortcode("shortcodedate format='m.d.y'", "shortcodedate_function");

function shortcodedate_function() {

  $date = getdate();

  return "$date[mon]".".$date[mday]".".$date[year]";
}
?>